#include <LiquidCrystal.h>
#include <NewPing.h>
#include <Servo.h>

#define totSensor 4
#define distMax 10
#define pingInterval 33

unsigned long pingTimer[totSensor];
unsigned int cm[totSensor];
uint8_t currentSensor = 0;

//sensor ldr
    int ldrInput = A1; 
    int ldrExit = A2;
    int valueInput = 0;
    int valueExit = 0;

//servo motor
    Servo motorInput;
    Servo motorExit;

//leds
    int pinLedRed1 = 6;
    int pinLedBlue1= 7;

    int pinLedRed2 = 4;
    int pinLedBlue2 = 5;

    int pinLedRed3 = 2;
    int pinLedGreen3 = 3;

    int pinLedRed4 = 44;
    int pinLedGreen4 = 45;
    
//contador de vagas
    int freeVacancies = 4;
    int occupiedVacancies = 0;

//vagas
    int sensor1 = 0;
    int sensor2 = 0;
    int sensor3 = 0;
    int sensor4 = 0;

//display
    LiquidCrystal lcd(8, 9, 10, 11, 12, 13);

//sensor ultrasonico
    NewPing sonar [totSensor] = { 
        NewPing( 38, 39, distMax),
        NewPing( 36, 37, distMax),
        NewPing( 34, 35, distMax),
        NewPing( 32, 33, distMax),
    };
 
void setup() { 
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.clear(); 

  pingTimer[0] = millis() + 75; 

  for (uint8_t i = 1; i < totSensor; i++)
  pingTimer[i] = pingTimer[i - 1] + pingInterval;

  pinMode(pinLedRed1, OUTPUT);
  pinMode(pinLedBlue1, OUTPUT);
  
  pinMode(pinLedRed2, OUTPUT);
  pinMode(pinLedBlue2, OUTPUT);
  
  pinMode(pinLedRed3, OUTPUT);
  pinMode(pinLedGreen3, OUTPUT);
  
  pinMode(pinLedRed4, OUTPUT);
  pinMode(pinLedGreen4, OUTPUT);
 

  motorInput.attach(53);
  motorExit.attach(52);
  pinMode(ldrInput, INPUT); 
  pinMode(ldrExit, INPUT);
  motorInput.write(10);
  motorExit.write(10);
}


void loop() {
  int valueInput = analogRead(ldrInput);
  int valueExit = analogRead(ldrExit);

  Serial.println("-------------");
  Serial.println("Valor ldrEntrada: ");
  Serial.print(valueInput);
  Serial.println("Valor ldrSaida: ");
  Serial.print(valueExit);
  Serial.println("-------------");

  if(valueInput < 300){
    motorInput.write(170); 
  } 
  else
  {
    motorInput.write(90); 
  }
  if(valueExit < 300){
    motorExit.write(170);
  }
  else{
    motorInput.write(90);
  }
  
  for(uint8_t i = 0; i < totSensor; i++)
  {
      if (millis() >= pingTimer[i])
        { 
            pingTimer[i] += pingInterval * totSensor;
            if (i == 0 && currentSensor == totSensor -1)oneSensorCycle(); 
                sonar[currentSensor].timer_stop();
                currentSensor = i;
                cm[currentSensor] = 0;
                sonar[currentSensor].ping_timer(echoCheck);
        }
    }

     occupiedVacancies = sensor1 + sensor2 + sensor3 + sensor4 ;
     freeVacancies = 4 - occupiedVacancies;
     lcd.setCursor(0,0);
     lcd.print("Vagas Livres = ");
     lcd.print(freeVacancies);
     lcd.setCursor(0,1);
     lcd.print("Vagas Ocupadas = ");
     lcd.print(occupiedVacancies);
}

void echoCheck()
{ 
  if(sonar[currentSensor].check_timer())
    cm[currentSensor] = sonar[currentSensor].ping_result / US_ROUNDTRIP_CM;
}

void oneSensorCycle()
{
    for (uint8_t i = 0; i< totSensor; i++)
    { 
        if(cm[0] > 1 && cm[0] < 50)
        {
            digitalWrite(pinLedRed1, 1);
            digitalWrite(pinLedBlue1, 0);
            sensor1 = 1;
        }   
        else
        {
            digitalWrite(pinLedRed1, 0); 
            digitalWrite(pinLedBlue1, 1);
            sensor1 = 0;
        }

        // --------- vaga 2 ----------
        if(cm[1] > 1 && cm[1] < 50) 
        { 
            digitalWrite(pinLedRed2, 1); //pino led Red
            digitalWrite(pinLedBlue2, 0); //pino Led Green
            sensor2 = 1;
        }
        else
        {
            digitalWrite(pinLedRed2, 0); //pino led Red
            digitalWrite(pinLedBlue2, 1); //pino Led Green
            sensor2 = 0;
        }
    
        // ---------- vaga 3 ----------
        if(cm[3] > 1 && cm[3] < 50) 
        { 
            digitalWrite(pinLedRed3, 1); //pino led Red
            digitalWrite(pinLedGreen3, 0); //pino Led Green
            sensor3 = 1;
        }
        else
        {
            digitalWrite(pinLedRed3, 0); //pino led Red
            digitalWrite(pinLedGreen3, 1); //pino Led Green
            sensor3 = 0;
        }

         //----------- vaga 4 ----------
         if(cm[4] > 1 && cm[4] < 50) 
        { 
            digitalWrite(pinLedRed4, 1); //pino led Red
            digitalWrite(pinLedGreen4, 0); //pino Led Green
            sensor4 = 1;
        }
        else
        {
            digitalWrite(pinLedRed4, 0); //pino led Red
            digitalWrite(pinLedGreen4, 1); //pino Led Green
            sensor4 = 0;
        }
        Serial.print("========");
        Serial.println("Sensor: ");
        Serial.print(i);
        Serial.print(" = ");
        Serial.print(cm[i]);
        Serial.print(" cm - ");
        Serial.println("========");
    }
    Serial.println();
}
